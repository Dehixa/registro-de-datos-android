package com.unsa.dnpa_lab07;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class FormularioDatos extends AppCompatActivity {

    Spinner cbo_distrito;
    Spinner cbo_provincia;
    Spinner cbo_departamento;
    Spinner cbo_pais;

    String distrito = "";
    String provincia = "";
    String departamento = "";
    String pais = "";

    TextInputEditText empresa;
    TextInputEditText ruc;
    TextInputEditText direccion;
    TextInputEditText representante;
    TextInputEditText correo;
    TextInputEditText telefono;
    TextInputEditText informacion;

    String listaDatos = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_datos);

        TextView titulo = findViewById(R.id.txt_Titulo);
        Button btnRegistrar = findViewById(R.id.btn_Registrar_Proveedor);

        TextInputLayout empresa_layout = findViewById(R.id.empresa_text_input);
        TextInputLayout ruc_layout = findViewById(R.id.ruc_text_input);
        TextInputLayout direccion_layout = findViewById(R.id.direccion_text_input);
        TextInputLayout representante_layout = findViewById(R.id.representante_text_input);
        TextInputLayout correo_layout = findViewById(R.id.correo_text_input);
        TextInputLayout telefono_layout = findViewById(R.id.telefono_text_input);
        TextInputLayout informacion_layout = findViewById(R.id.informacion_text_input);

        empresa = findViewById(R.id.editTextEmpresa);
        ruc = findViewById(R.id.editTextRuc);
        direccion = findViewById(R.id.editTextDireccion);
        representante = findViewById(R.id.editTextRepresentante);
        correo = findViewById(R.id.editTextCorreo);
        telefono = findViewById(R.id.editTextTelefono);
        informacion = findViewById(R.id.editTextInformacion);

        cbo_distrito = findViewById(R.id.cbo_Distrito);
        cbo_provincia = findViewById(R.id.cbo_Provincia);
        cbo_departamento = findViewById(R.id.cbo_Departamento);
        cbo_pais = findViewById(R.id.cbo_Pais);

        cbo_distrito.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                distrito = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {    }
        });

        cbo_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                provincia = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {    }
        });

        cbo_departamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                departamento = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {    }
        });

        cbo_pais.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                pais = (String) adapterView.getItemAtPosition(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {    }
        });

        inicializarSpinners();

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Proveedor proveedor = new Proveedor();
                proveedor.setEmpresa(empresa.getText().toString());
                proveedor.setRuc(ruc.getText().toString());
                proveedor.setDireccion(direccion.getText().toString());
                proveedor.setDistrito(distrito);
                proveedor.setProvincia(provincia);
                proveedor.setDepartamento(departamento);
                proveedor.setPais(pais);
                proveedor.setRepresentante(representante.getText().toString());
                proveedor.setCorreo(correo.getText().toString());
                proveedor.setTelefono(telefono.getText().toString());
                proveedor.setInformacion(informacion.getText().toString());

                String json = proveedor.toJSON();
                System.out.println("++++++++++++++++++++++++++++++++++++++++++");
                System.out.println(json);



                new HttpTask(new AsyncTaskListener() {
                    @Override
                    public void onHttpTask(String data) {
                        System.out.println("******************************************");
                        System.out.println(data);
                        listaDatos = data;
                    }
                }).execute(HostURL.SERVER, json);

                String msg = "Registro Exitoso.";
                Toast.makeText(FormularioDatos.this, msg, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(FormularioDatos.this, ListaDatos.class);
                intent.putExtra("datos", listaDatos);
                startActivity(intent);
            }
        });
    }

    private void inicializarSpinners() {
        String[] distritos = {"---Distrito---", "Alto Selva Alegre", "Arequipa", "Cayma", "Cerro Colorado", "Characato",
                                "Chiguata", "Jacobo Hunter", "Jose Luis Bustamante y Rivero", "La Joya",
                                "Mariano Melgar", "Miraflores", "Mollebaya", "Paucarpata", "Pocsi", "Polobaya",
                                "Quequeña", "Sabandia", "Sachaca", "San Juan de Siguas", "San Juan de Tarucani",
                                "Santa Isabel de Siguas", "Santa Rita de Siguas", "Socabaya", "Tiabaya", "Uchumayo",
                                "Vitor", "Yanahuara", "Yarabamba", "Yura"};
        cbo_distrito.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, distritos));

        String[] provincias = {"---Provincia---", "Arequipa"};
        cbo_provincia.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, provincias));

        String[] departamentos = {"---Departamento---", "Arequipa"};
        cbo_departamento.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, departamentos));

        String[] paises = {"---País---", "Perú"};
        cbo_pais.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paises));

    }
}
