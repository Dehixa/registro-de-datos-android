package com.unsa.dnpa_lab07;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

// NO SIRVE, NO ES USADA 

public class DataAdapter extends ArrayAdapter<Proveedor> {

    Context contexto;
    ArrayList<Proveedor> items;
    private LayoutInflater inflater;

    public DataAdapter(Context context, int resource, ArrayList<Proveedor> objects) {
        super(context, resource, objects);
        items = objects;
        inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Proveedor getItem(int index) {
        return items.get(index);
    }

    @Override
    public long getItemId(int index) {
        return 0;
    }

    @Override
    public View getView(int index, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.fila_proveedor, parent, false);
        }

        Proveedor item = getItem(index);

        TextView empresa = (TextView) view.findViewById(R.id.txt_Empresa);
        TextView ruc = (TextView) view.findViewById(R.id.txt_Ruc);
        TextView direccion = (TextView) view.findViewById(R.id.txt_Direccion);
        TextView representante = (TextView) view.findViewById(R.id.txt_Representante);
        TextView correo = (TextView) view.findViewById(R.id.txt_Correo);
        TextView telefono = (TextView) view.findViewById(R.id.txt_Telefono);
        TextView informacion = (TextView) view.findViewById(R.id.txt_Informacion);

        empresa.setText(item.getEmpresa());
        ruc.setText(item.getRuc());
        String address = item.getDireccion() + " - " + item.getDistrito() + " - Provincia: "
                + item.getProvincia() + " - Departamento: " + item.getDepartamento()
                + " - País: " + item.getPais();
        direccion.setText(address);
        representante.setText(item.getRepresentante());
        correo.setText(item.getCorreo());
        telefono.setText(item.getTelefono());
        informacion.setText(item.getInformacion());

        return view;

    }
}