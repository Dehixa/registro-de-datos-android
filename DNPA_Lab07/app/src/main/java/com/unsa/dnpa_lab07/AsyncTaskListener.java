package com.unsa.dnpa_lab07;

/* Interfaz del Listener personalizado*/

public interface AsyncTaskListener {
    public void onHttpTask(String data);
}
