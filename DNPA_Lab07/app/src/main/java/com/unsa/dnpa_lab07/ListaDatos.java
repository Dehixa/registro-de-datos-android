package com.unsa.dnpa_lab07;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

// NO SIRVE, NO ES USADA

public class ListaDatos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_datos);

        initListView(getProveedores());

        Button btnRegresar = findViewById(R.id.btn_Regresar);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListaDatos.this, MenuPrincipal.class);
                startActivity(intent);
            }
        });

    }

    private void initListView(ArrayList<Proveedor> proveedores) {

        final ListView listView = (ListView) findViewById(R.id.listProveedores);

        DataAdapter adapter = new DataAdapter(getApplicationContext(), R.id.listProveedores, proveedores);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                //Document item = (Document) listView.getItemAtPosition(position);
            }
        });

    }

    private ArrayList<Proveedor> getProveedores() {

        ArrayList<Proveedor> list = new ArrayList<>();
        Proveedor proveedor = null;

        String data = getIntent().getExtras().getString("datos");;

        try {

            JSONArray jsonArray = new JSONArray(data);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject row = jsonArray.getJSONObject(i);
                proveedor = new Proveedor();
                proveedor.setEmpresa(row.getString("empresa"));
                proveedor.setRuc(row.getString("ruc"));
                proveedor.setDireccion(row.getString("direccion"));
                proveedor.setDistrito(row.getString("distrito"));
                proveedor.setProvincia(row.getString("provincia"));
                proveedor.setDepartamento(row.getString("departamento"));
                proveedor.setPais(row.getString("pais"));
                proveedor.setRepresentante(row.getString("representante"));
                proveedor.setCorreo(row.getString("correo"));
                proveedor.setTelefono(row.getString("telefono"));
                proveedor.setInformacion(row.getString("informacion"));
                list.add(proveedor);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }
}