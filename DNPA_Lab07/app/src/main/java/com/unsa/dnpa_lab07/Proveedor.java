package com.unsa.dnpa_lab07;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Proveedor implements Serializable{
    private String empresa;
    private String ruc;
    private String direccion;
    private String distrito;
    private String provincia;
    private String departamento;
    private String pais;
    private String representante;
    private String correo;
    private String telefono;
    private String informacion;

    public Proveedor (){ }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getRepresentante() {
        return representante;
    }

    public void setRepresentante(String representante) {
        this.representante = representante;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getInformacion() {
        return informacion;
    }

    public void setInformacion(String informacion) {
        this.informacion = informacion;
    }

    public String toJSON() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("empresa", getEmpresa());
            jsonObject.put("ruc", getRuc());
            jsonObject.put("direccion", getDireccion());
            jsonObject.put("distrito", getDistrito());
            jsonObject.put("provincia", getProvincia());
            jsonObject.put("departamento", getDepartamento());
            jsonObject.put("pais", getPais());
            jsonObject.put("representante", getRepresentante());
            jsonObject.put("correo", getCorreo());
            jsonObject.put("telefono", getTelefono());
            jsonObject.put("informacion", getInformacion());

            return jsonObject.toString();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return "";
        }

    }

}