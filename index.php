<?php

# Recibimos los datos leídos de php://input
$datosRecibidos = file_get_contents("php://input");
# No los hemos decodificado, así que lo hacemos de una vez:
$proveedor = json_decode($datosRecibidos);
# Ahora podemos acceder a los datos, accederemos a unos pocos para demostrar
$empresa = $proveedor->empresa;
$ruc = $proveedor->ruc;
$direccion = $proveedor->direccion;
$distrito = $proveedor->distrito;
$provincia = $proveedor->provincia;
$departamento = $proveedor->departamento;
$pais = $proveedor->pais;
$representante = $proveedor->representante;
$correo = $proveedor->correo;
$telefono = $proveedor->telefono;
$informacion = $proveedor->informacion;

#conexion a base de datos
$usuario = "root";
$contrasena = "";
$servidor = "localhost";
$basededatos = "proveedores";

$conexion = mysqli_connect($servidor, $usuario, $contrasena, $basededatos) or die ("No se pudo conectar.");

$sql = "INSERT INTO proveedor (Empresa, Ruc, Direccion, Distrito, Provincia, Departamento, Pais, Representante, Correo, Telefono, Informacion) VALUES ('$empresa', '$ruc', '$direccion', '$distrito', '$provincia', '$departamento', '$pais', '$representante', '$correo', '$telefono', '$informacion')";

if (mysqli_query($conexion, $sql)) {
      echo "New record created successfully";
} else {
	echo "Error: " . $sql . "<br>" . mysqli_error($conexion);
}
mysqli_close($conexion);

# Finalmente armamos la respuesta, igualmente como JSON
# sería como un espejo pero en otras circunstancias podrías devolver
# datos de una base de datos u otro medio
$respuesta = [
    "mensaje" => "He recibido los datos",
];
# Codificarla e imprimirla
$respuestaCodificada = json_encode($respuesta);
echo $respuestaCodificada;
# A partir de aquí no se debe imprimir otra cosa porque "ensuciaría" la respuesta
?>
