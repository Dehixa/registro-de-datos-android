-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 17-06-2019 a las 17:51:25
-- Versión del servidor: 10.1.37-MariaDB
-- Versión de PHP: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proveedores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `ID` int(4) NOT NULL,
  `Empresa` varchar(100) NOT NULL,
  `Ruc` varchar(11) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Distrito` varchar(40) NOT NULL,
  `Provincia` varchar(40) NOT NULL,
  `Departamento` varchar(40) NOT NULL,
  `Pais` varchar(40) NOT NULL,
  `Representante` varchar(100) NOT NULL,
  `Correo` varchar(50) NOT NULL,
  `Telefono` varchar(9) NOT NULL,
  `Informacion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`ID`, `Empresa`, `Ruc`, `Direccion`, `Distrito`, `Provincia`, `Departamento`, `Pais`, `Representante`, `Correo`, `Telefono`, `Informacion`) VALUES
(1, 'hfhcjfjf', '656864', 'bdhxjd', '---Distrito---', '---Provincia---', '---Departamento---', '---PaÃ­s---', 'hducj', 'ydjfj@dbf', '56861', 'xbjckdi'),
(2, '', '', '', '', '', '', '', '', '', '', ''),
(3, 'cate', '48543464', 'calle cuba', 'Jose Luis Bustamante y Rivero', 'Arequipa', 'Arequipa', 'PerÃº', 'karla', 'dehixa1997@gmail.com', '94676468', 'productos');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
